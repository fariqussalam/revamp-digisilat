var KategoriType = {
    KELAS: "KELAS",
    KATEGORI: "KATEGORI",
    JABATAN: "JABATAN",
    KONTINGEN: "KONTINGEN"
}

KategoriType.toLabel = function (tipe) {
    let label;
    switch (tipe) {
        case KategoriType.KELAS:
            label = "Kelas";
            break;
        case KategoriType.KATEGORI:
            label = "Kategori Seni";
            break;
        case KategoriType.JABATAN:
            label = "Jabatan";
            break;
        case KategoriType.KONTINGEN:
            label = "Kontingen";
            break;
        default:
            label = "Kategori";
    }
    return label;
}

module.exports = KategoriType;