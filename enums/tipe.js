var PesertaType = {
    TANDING: "TANDING",
    SENI: "SENI",
    OFFICIAL: "OFFICIAL"
}

PesertaType.toLabel = function (tipe) {
    let label;
    switch (tipe) {
        case PesertaType.TANDING:
            label = "Tanding";
            break;
        case PesertaType.SENI:
            label = "Seni";
            break;
        case PesertaType.OFFICIAL:
            label = "Official";
            break;
        default:
            label = "Tanding";
    }
    return label;
}


module.exports = PesertaType;