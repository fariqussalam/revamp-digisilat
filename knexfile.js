module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: './digisilat.sqlite3'
    },
    useNullAsDefault: true
  }

};
