
var KategoriType = require('../enums/tipeKategori');
var PesertaType = require('../enums/tipe');
exports.seed = function(knex) {
  return knex('peserta').del()
  .then(function () {
    return knex('kategori').del()
  })
  .then(function () {
    return knex('turnamen').del()
  })
  .then(function () {
    return knex('setting').del()
  })
  .then(function () {
    return knex('turnamen').insert([
      {
        id: 1,
        nama: "Pra Porda Selayar 2020",
        waktu: "Februari - Maret 2020",
        tempat: "Selayar, Sulawesi Selatan",
        is_active: true
      }
    ]);
  })
  .then(function () {
    return knex('kategori').insert([
      {id: 1, nama: 'A Putra', tipe: KategoriType.KELAS, turnamen_id: 1},
      {id: 2, nama: 'Tunggal Putra', tipe: KategoriType.KATEGORI, turnamen_id: 1},
      {id: 3, nama: 'Manager', tipe: KategoriType.JABATAN, turnamen_id: 1},
      {id: 4, nama: 'Sulawesi Selatan', tipe: KategoriType.KONTINGEN, turnamen_id: 1},
    ]);
  })  
  .then(function () {
    return knex('peserta').insert([
      {id: 1, nama: 'Andi Baso', tipe: PesertaType.TANDING, turnamen_id: 1, kelas_id: 1, kontingen_id: 1},
      {id: 2, nama: 'Mayong Adi Wardana', tipe: PesertaType.SENI, turnamen_id: 1, kategori_id: 1, kontingen_id: 1},
      {id: 3, nama: 'Muhammad Johan', tipe: PesertaType.OFFICIAL, turnamen_id: 1, jabatan_id: 1, kontingen_id: 1},  
    ]);
    });
};
