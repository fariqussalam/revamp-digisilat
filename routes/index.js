var express = require('express')
var router = express.Router()
const service = require('../services/dataService')

router.get('/', async function (req, res, next) {
  let turnamenList = await service.list("turnamen")
  console.log(turnamenList);
  res.render('turnamen/index', {
    title: 'Daftar Turnamen',
    turnamenList
  });
})

module.exports = router