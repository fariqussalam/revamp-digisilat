const router = require('express').Router()
const service = require('../services/dataService')

router.get('/', async function (req, res, next) {
  let turnamenList = await service.list("turnamen")

  res.render('turnamen/index', {
    title: 'Daftar Turnamen',
    turnamenList
  });

});

router.get('/create', function (req, res, next) {
  res.render('turnamen/form', {
    title: 'Create Turnamen'
  })
});

router.get('/edit/:id', async function (req, res, next) {
  let turnamen = await service.getTurnamen(req.params.id);
  console.log(turnamen);
  res.render('turnamen/form', {
    title: 'Edit Turnamen',
    turnamen: turnamen,
    action: 'edit'
  })
});

router.post("/save", async function (req, res, next) {
  let {
    nama,
    waktu,
    tempat
  } = req.body;

  await service.createTurnamen({
    nama,
    waktu,
    tempat
  });

  req.flash("message", "Tambah Turnamen Berhasil");
  res.redirect("/turnamen");
});

router.post("/update", async function (req, res, next) {
  let {
    nama,
    waktu,
    tempat,
    id
  } = req.body;

  await service.updateTurnamen(id, {
    nama,
    waktu,
    tempat
  });

  req.flash("message", "Edit Turnamen Berhasil");
  res.redirect("/turnamen");
});

router.get("/delete/:id", async function (req, res, next) {
  let id = req.params.id;
  try {
    await service.delete("turnamen", id);
    req.flash("message", "Hapus Turnamen Berhasil");
  } catch (exception) {
    req.flash("error", exception.message);
  }

  res.redirect("/turnamen");
});

module.exports = router