const router = require('express').Router()
const service = require('../services/dataService')
const _ = require('lodash');
const KategoriType = require("../enums/tipeKategori");
const PesertaType = require("../enums/tipe");

router.get("/", async function (req, res, next) {

    let pesertaTanding = await service.listPesertaTanding();
    let pesertaSeni = await service.listPesertaSeni();
    let officialList = await service.officialList();

    console.log(pesertaTanding);
    res.render("peserta/list", {
        pesertaTanding,
        pesertaSeni,
        officialList
    })
});

router.get("/create", async function (req, res) {
    let tipe = req.query.tipe;
    let label = PesertaType.toLabel(tipe);
    let turnamenList = await service.list("turnamen");

    let kategoriList = await service.listKategori();
    let kontingenList = _.filter(kategoriList, function (el) {
        return el.tipe == KategoriType.KONTINGEN
    });
    let kelasList = _.filter(kategoriList, function (el) {
        return el.tipe == KategoriType.KELAS
    });
    let jabatanList = _.filter(kategoriList, function (el) {
        return el.tipe == KategoriType.JABATAN
    });
    let kategoriSeniList = _.filter(kategoriList, function (el) {
        return el.tipe == KategoriType.KATEGORI
    });
    res.render("kategori/form", {
        tipe: tipe,
        label: label,
        turnamenList
    });
})

router.get("/edit/:id", async function (req, res, next) {
    let id = req.params.id;
    let turnamenList = await service.list("turnamen");
    let kategori = await service.get("kategori", id);

    res.render("kategori/form", {
        kategori,
        turnamenList,
        action: 'edit'
    })
});

router.get("/delete/:id", async function (req, res, next) {
    let id = req.params.id;

    try {
        await service.delete("kategori", id);
        req.flash("message", "Hapus Kategori Berhasil");
    } catch (exception) {
        req.flash("error", exception.message);
    }

    res.redirect("/kategori");
});

router.post("/save", async function (req, res, next) {
    let {
        tipe,
        nama,
        turnamen
    } = req.body;

    let turnamenObject = await service.getTurnamen(turnamen);
    let success = await service.createKategori({
        tipe: tipe,
        nama: nama,
        turnamen_id: turnamenObject.id
    });

    req.flash("message", "Buat Kategori Berhasil");
    res.redirect("/kategori");
});

router.post("/update", async function (req, res, next) {
    let {
        id,
        tipe,
        nama,
        turnamen
    } = req.body;

    let turnamenObject = await service.getTurnamen(turnamen);
    let success = await service.updateKategori(id, {
        tipe: tipe,
        nama: nama,
        turnamen_id: turnamenObject.id
    });

    req.flash("message", "Buat Kategori Berhasil");
    res.redirect("/kategori");
});
module.exports = router