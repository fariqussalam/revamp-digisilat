const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const twig = require('twig');
const flash = require('connect-flash');
const session = require('express-session')
const service = require('./services/dataService');

/**
 * Express Setup
 */
const app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');
twig.cache(false);
app.set('view cache', false);
app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: false
  }
}));
app.use(flash());

app.use(async function (req, res, next) {
  res.locals.flash = req.flash();
  res.locals.url = req.originalUrl;
  res.locals.turnamenAktif = await service.getTurnamenAktif();
  next();
});


/**
 * Controller Setup
 */
const turnamenRoutes = require('./routes/turnamen');
const kategoriRoutes = require('./routes/kategori');
const indexRoutes = require('./routes/index');
const pesertaRoutes = require('./routes/peserta');

app.use('/', indexRoutes);
app.use('/turnamen', turnamenRoutes);
app.use('/kategori', kategoriRoutes);
app.use('/peserta', pesertaRoutes);

/**
 * Error Handler
 */
app.use(function (req, res, next) {
  next(createError(404));
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;