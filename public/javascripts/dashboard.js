/* globals Chart:false, feather:false */

(function () {
  'use strict'
  $(document).ready(function () {
    $('#example').DataTable({
      "lengthChange": false,
      "columnDefs": [{
        "width": "5%",
        "targets": 0
      }]
    });
    $('.table-kategori').DataTable({
      "lengthChange": false,
      "columnDefs": [{
        "width": "5%",
        "targets": 0
      }]
    });

    $('.table-peserta').DataTable({
      "lengthChange": false,
      "columnDefs": [{
        "width": "5%",
        "targets": 0
      }]
    });

    $('.js-select2').select2();

    $('.js-btn-delete').click(function () {
      var url = $(this).data("url");
      var message = $(this).data("message");
      bootbox.confirm({
        message: message,
        buttons: {
          confirm: {
            label: 'Yes',
            className: 'btn-success'
          },
          cancel: {
            label: 'No',
            className: 'btn-danger'
          }
        },
        callback: function (result) {
          if (result) {
            window.location.href = url;
          }
        }
      });
    });

  });
}())