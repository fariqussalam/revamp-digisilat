exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('turnamen', function(table) {
      table.increments();
      table.boolean('is_active').notNullable().defaultTo(false);
      table.string('nama').notNullable();
      table.string('waktu').nullable();
      table.string('tempat').nullable();
      table.timestamp('created_at').defaultTo(knex.fn.now())
      table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
    .createTable('kategori', function(table) {
        table.increments();
        table.string('nama').notNullable();
        table.string('tipe').notNullable();
        table.string('turnamen_id').references('id').inTable('turnamen').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
      })
    .createTable('peserta', function(table) {
        table.increments();
        table.string('nama').notNullable();
        table.integer('kontingen_id').references('id').inTable('kategori').nullable();
        table.string('kelas_id').references('id').inTable('kategori').nullable();
        table.string('kategori_id').references('id').inTable('kategori').nullable();
        table.string('jabatan_id').references('id').inTable('kategori').nullable();
        table.string('tipe').notNullable();
        table.string('turnamen_id').references('id').inTable('turnamen').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
      })
    .createTable('setting', function(table) {
        table.increments();
        table.string('setting_code').notNullable();
        table.string('setting_value').nullable();
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    });
  }
  
  exports.down = function(knex, Promise) {
    return knex.schema
    .dropTable('turnamen')
    .dropTable('peserta')
    .dropTable('kategori')
    .dropTable('setting');
  }