const db = require('../knex/knex');
let service = {}
const _ = require("lodash");

service.list = async function (table) {
    let result = await db(table).select();
    return result;
};
service.get = async function (table, id) {
    let result = await db(table).where("id", id);
    return result[0];
};
service.create = async function (table, data) {
    let result = await db(table).insert(data);
    return result;
}
service.delete = async function (table, id) {
    let result = await db(table).where("id", id).del();
    return result;
}

service.getTurnamen = async function (id) {
    let result = await db("turnamen").where("id", id);
    return result[0];
}
service.createTurnamen = async function (data) {
    let result = await db("turnamen").insert(data);
    return result;
}
service.updateTurnamen = async function (id, data) {
    let result = await db("turnamen").where("id", id).update(data);
    return result;
}
service.deleteTurnamen = async function (id) {
    let result = await db("turnamen").where("id", id).del();
    return result;
}

service.createKategori = async function (kategori) {
    let result = await db("kategori").insert(kategori);
    return result;
}

service.updateKategori = async function (id, data) {
    let result = await db("kategori").where("id", id).update(data);
    return result;
}

service.listKategori = async function () {
    let list = []
    let kategoriList = await service.list("kategori");
    for (let el of kategoriList) {
        let turnamen = await service.getTurnamen(el.turnamen_id);
        list.push({
            id: el.id,
            nama: el.nama,
            turnamen: turnamen,
            created_at: el.created_at,
            tipe: el.tipe
        });
    }

    return list;
}

service.listPesertaTanding = async function () {
    let listPeserta = []
    let result = await db("peserta").where("tipe", "TANDING");
    for (let el of result) {
        let turnamen = await service.getTurnamen(el.turnamen_id);
        let kontingen = await service.get("kategori", el.kontingen_id);
        let kelas = await service.get("kategori", el.kelas_id);
        let peserta = {
            id: el.id,
            nama: el.nama,
            kontingen: kontingen.nama,
            turnamen: turnamen.nama,
            kelas: kelas.nama
        }
        listPeserta.push(peserta)
    }

    return listPeserta;
}

service.listPesertaSeni = async function () {
    let listPeserta = []
    let result = await db("peserta").where("tipe", "SENI");
    for (let el of result) {
        let turnamen = await service.getTurnamen(el.turnamen_id);
        let kontingen = await service.get("kategori", el.kontingen_id);
        let kategori = await service.get("kategori", el.kategori_id);
        let peserta = {
            id: el.id,
            nama: el.nama,
            kontingen: kontingen.nama,
            turnamen: turnamen.nama,
            kategori: kategori.nama
        }
        listPeserta.push(peserta)
    }

    return listPeserta;
}

service.officialList = async function () {
    let listPeserta = []
    let result = await db("peserta").where("tipe", "OFFICIAL");
    for (let el of result) {
        let turnamen = await service.getTurnamen(el.turnamen_id);
        let kontingen = await service.get("kategori", el.kontingen_id);
        let jabatan = await service.get("kategori", el.jabatan_id);
        let peserta = {
            id: el.id,
            nama: el.nama,
            kontingen: kontingen.nama,
            turnamen: turnamen.nama,
            jabatan: jabatan.nama
        }
        listPeserta.push(peserta)
    }

    return listPeserta;
}

service.getTurnamenAktif = async function () {
    let result = await db("turnamen").where("is_active", true).first();
    if (!result) return null;
    return result[0];
}

module.exports = service;